package eu.bcvsolutions.idm.document.entity;

public enum DocType {
	PASSPORT,
	ID_CARD,
	DRIVER_LICENSE
}
