package eu.bcvsolutions.idm.document.config.domain;

import org.springframework.stereotype.Component;

import eu.bcvsolutions.idm.core.api.config.domain.AbstractConfiguration;

/**
 * Example configuration - implementation
 *
 * @author Radek Tomiška
 */
@Component("documentConfiguration")
public class DefaultDocumentConfiguration
		extends AbstractConfiguration
		implements DocumentConfiguration {

	@Override
	public String getPrivateValue() {
		return getConfigurationService().getValue(PROPERTY_PRIVATE);
	}

	@Override
	public String getConfidentialValue() {
		return getConfigurationService().getValue(PROPERTY_CONFIDENTIAL);
	}
}
