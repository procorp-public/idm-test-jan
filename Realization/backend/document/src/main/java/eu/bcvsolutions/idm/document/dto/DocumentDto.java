package eu.bcvsolutions.idm.document.dto;

import java.io.Serializable;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import eu.bcvsolutions.idm.document.entity.Document;
import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class DocumentDto implements Serializable {

	@NotBlank
	@NotEmpty
	private UUID uuid;

	@NotBlank
	@NotEmpty
	private String type;

	@NotBlank
	@NotEmpty
	private Long number;

	@NotBlank
	@NotEmpty
	private String firstName;

	@NotBlank
	@NotEmpty
	private String lastName;

	@NotBlank
	@NotEmpty
	private String state;

	public DocumentDto(Document document) {
		this(
				document.getUuid(),
				document.getType().toString(),
				document.getNumber(),
				document.getFirstName(),
				document.getLastName(),
				document.getState().toString());
	}
}
