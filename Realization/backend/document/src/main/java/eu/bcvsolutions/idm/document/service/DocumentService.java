package eu.bcvsolutions.idm.document.service;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import eu.bcvsolutions.idm.core.api.dto.IdmIdentityDto;
import eu.bcvsolutions.idm.core.model.entity.IdmIdentity;
import eu.bcvsolutions.idm.core.model.repository.IdmIdentityRepository;
import eu.bcvsolutions.idm.document.dto.DocumentDto;
import eu.bcvsolutions.idm.document.entity.DocState;
import eu.bcvsolutions.idm.document.entity.DocType;
import eu.bcvsolutions.idm.document.entity.Document;
import eu.bcvsolutions.idm.document.repository.DocumentRepository;
import lombok.AllArgsConstructor;

@Service
@Validated
@AllArgsConstructor
public class DocumentService {

	private final DocumentRepository documentRepository;
	private final IdmIdentityRepository identityRepository;

	public DocumentDto getDocDtoByUuid(UUID uuid) {
		return new DocumentDto(documentRepository.getOne(uuid));
	}

	public Optional<Document> getDocByNumber(Long number) {
		return documentRepository.findByNumber(number);
	}

	public DocumentDto create(@Valid DocumentDto documentDto, IdmIdentityDto identityDto) {
		if (!isEnumValid(documentDto)) {
			return null; //TODO
		}
		IdmIdentity identity = identityRepository.findOneByUsername(identityDto.getUsername());
		Document document = documentRepository.save(new Document(documentDto, identity));
		return new DocumentDto(document);
	}

	public DocumentDto update(DocumentDto documentDto) {
		Document document = documentRepository.getOne(documentDto.getUuid());
		document.update(documentDto);
		return new DocumentDto(documentRepository.save(document));
	}

	public void deleteByUuid(UUID documentUuid) {
		documentRepository.deleteById(documentUuid);
	}

	public void deleteDocument(Document document) {
		documentRepository.delete(document);
	}

	private boolean isEnumValid(DocumentDto documentDto) {
		return Arrays.stream(DocType.values())
				.map(Enum::toString)
				.anyMatch(str -> str.equals(documentDto.getType()))
				&& Arrays.stream(DocState.values())
				.map(Enum::toString)
				.anyMatch(str -> str.equals(documentDto.getState()));
	}
}
