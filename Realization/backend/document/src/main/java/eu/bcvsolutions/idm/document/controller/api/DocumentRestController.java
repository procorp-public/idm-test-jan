package eu.bcvsolutions.idm.document.controller.api;

import java.util.Optional;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.bcvsolutions.idm.core.api.dto.IdmIdentityDto;
import eu.bcvsolutions.idm.document.dto.DocumentDto;
import eu.bcvsolutions.idm.document.entity.Document;
import eu.bcvsolutions.idm.document.service.DocumentService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/doc")
public class DocumentRestController {

	private final DocumentService service;

	@PostMapping
	public ResponseEntity<?> create(@RequestBody DocumentDto documentDto, @RequestBody IdmIdentityDto identityDto) {
		DocumentDto newDocumentDto = service.create(documentDto, identityDto);
		return ResponseEntity.ok(newDocumentDto);
	}

	@GetMapping("/{documentId}")
	public ResponseEntity<DocumentDto> read(@PathVariable UUID documentId) {
		DocumentDto documentDto = service.getDocDtoByUuid(documentId);
		return documentDto == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(documentDto);
	}

	@PutMapping
	public ResponseEntity<?> update(@RequestBody DocumentDto documentDto) {
		DocumentDto newDocDto = service.update(documentDto);
		return ResponseEntity.ok(newDocDto);
	}

	@DeleteMapping("/{documentId}")
	public ResponseEntity<?> delete(@PathVariable UUID documentId) {
		service.deleteByUuid(documentId);
		return ResponseEntity.ok(String.format("Document with ID %s has been deleted", documentId));
	}

	@DeleteMapping("/{documentNumber}")
	public ResponseEntity<?> delete(@PathVariable Long documentNumber) {
		Optional<Document> optDocument = service.getDocByNumber(documentNumber);
		if (optDocument.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		service.deleteDocument(optDocument.get());
		return ResponseEntity.ok(String.format("Document with number %s has been deleted", documentNumber));
	}
}
