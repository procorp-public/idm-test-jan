package eu.bcvsolutions.idm.document.event.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Description;

import eu.bcvsolutions.idm.core.api.dto.IdmIdentityDto;
import eu.bcvsolutions.idm.core.api.event.CoreEventProcessor;
import eu.bcvsolutions.idm.core.api.event.DefaultEventResult;
import eu.bcvsolutions.idm.core.api.event.EntityEvent;
import eu.bcvsolutions.idm.core.api.event.EventResult;
import eu.bcvsolutions.idm.core.api.event.processor.IdentityProcessor;
import eu.bcvsolutions.idm.core.model.event.IdentityEvent;
import eu.bcvsolutions.idm.document.repository.DocumentRepository;
import lombok.Getter;

@Getter
@Component("documentDeleteSyncProcessor")
@Description("Deletes all documents when identity is deleted")
public class DocumentProcessor extends CoreEventProcessor<IdmIdentityDto>
		implements IdentityProcessor {

	private final DocumentRepository documentRepository;

	@Autowired
	public DocumentProcessor(DocumentRepository documentRepository) {
		super(IdentityEvent.IdentityEventType.DELETE);
		this.documentRepository = documentRepository;
	}

	@Override
	public EventResult<IdmIdentityDto> process(EntityEvent<IdmIdentityDto> event) {
		IdmIdentityDto deletedIdentity = event.getContent();
		documentRepository.deleteAllByIdentity_Id(deletedIdentity.getId());
		return new DefaultEventResult<>(event, this);
	}

}
