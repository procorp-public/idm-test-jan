package eu.bcvsolutions.idm.document.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import eu.bcvsolutions.idm.core.api.entity.AbstractEntity;
import eu.bcvsolutions.idm.core.model.entity.IdmIdentity;
import eu.bcvsolutions.idm.document.dto.DocumentDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Document extends AbstractEntity {

	@NotNull
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private DocType type;

	@NotNull
	private Long number;

	@NotNull
	private String firstName;

	@NotNull
	private String lastName;

	@NotNull
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private DocState state;

	@ManyToOne
	private IdmIdentity identity;


	public Document(DocumentDto dto, IdmIdentity identity) {
		this.type = DocType.valueOf(dto.getType());
		this.number = dto.getNumber();
		this.firstName = dto.getFirstName();
		this.lastName = dto.getLastName();
		this.state = DocState.valueOf(dto.getState());
		this.identity = identity;
	}

	public void update(DocumentDto dto) {
		this.type = DocType.valueOf(dto.getType());
		this.number = dto.getNumber();
		this.firstName = dto.getFirstName();
		this.lastName = dto.getLastName();
		this.state = DocState.valueOf(dto.getState());
	}
}
