package eu.bcvsolutions.idm.document.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.bcvsolutions.idm.core.model.entity.IdmIdentity;
import eu.bcvsolutions.idm.document.entity.Document;

@Repository
public interface DocumentRepository extends JpaRepository<Document, UUID> {

	Optional<Document> findByNumber(Long number);

	void deleteAllByIdentity_Id(UUID uuid);
}
